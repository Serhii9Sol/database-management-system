package com.services.dbServices.impl;

import com.models.Person;
import com.mongodb.client.*;
import com.mongodb.client.model.Updates;
import org.bson.Document;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import java.util.Map;
import static com.mongodb.client.model.Filters.eq;
import static org.junit.jupiter.api.Assertions.*;

class MongoDBServiceTest {

    private static final String DB_URL = "mongodb://127.0.0.1:27017";
    private static final String DB_NAME = "mongo-database";
    private static final String COLLECTION_NAME = "persons";
    private static final String ID = "id";
    private static final String F_NAME = "fName";
    private static final String L_NAME = "lName";
    private static final String AGE = "age";
    private static final String CITY = "city";
    private final MongoClient client = Mockito.mock(MongoClient.class);
    private final MongoDatabase dataBase = Mockito.mock(MongoDatabase.class);
    private final MongoCollection<Document> collection = Mockito.mock(MongoCollection.class);
    private final FindIterable<Document> documents = Mockito.mock(FindIterable.class);
    private final Person testPerson = new Person(1, "Vasiliy", "Pupko", 33, "Kiev");
    private final Document testDocument = new Document(Map.of(ID, testPerson.getId(), F_NAME, testPerson.getFname(),
            L_NAME, testPerson.getLname(), AGE, testPerson.getAge(), CITY, testPerson.getCity()));

    private final MongoDBService cut = new MongoDBService();

    @Test
    void createTest() {
        try (MockedStatic<MongoClients> mockMongoClients = Mockito.mockStatic(MongoClients.class)) {
            mockMongoClients.when(() -> MongoClients.create(DB_URL)).thenReturn(client);
            Mockito.when(client.getDatabase(DB_NAME)).thenReturn(dataBase);
            Mockito.when(dataBase.getCollection(COLLECTION_NAME)).thenReturn(collection);

            cut.create(testPerson);

            Mockito.verify(collection, Mockito.times(1)).insertOne(testDocument);
        }
    }

    @Test
    void createNullPersonTest() {
        try (MockedStatic<MongoClients> mockMongoClients = Mockito.mockStatic(MongoClients.class)) {
            mockMongoClients.when(() -> MongoClients.create(DB_URL)).thenReturn(client);
            Mockito.when(client.getDatabase(DB_NAME)).thenReturn(dataBase);
            Mockito.when(dataBase.getCollection(COLLECTION_NAME)).thenReturn(collection);

            assertDoesNotThrow(() -> cut.create(null));
            Mockito.verifyNoInteractions(collection);
            Mockito.verifyNoInteractions(dataBase);
            Mockito.verifyNoInteractions(client);
        }
    }

//    @Test
//    void readTest() {
//        MockedStatic<MongoClients> mockMongoClients = Mockito.mockStatic(MongoClients.class);
//        mockMongoClients.when(() -> MongoClients.create(DB_URL)).thenReturn(client);
//        Mockito.when(client.getDatabase(DB_NAME)).thenReturn(dataBase);
//        Mockito.when(dataBase.getCollection(COLLECTION_NAME)).thenReturn(collection);
//        Mockito.when(collection.find()).thenReturn(documents);
//        Mockito.when(documents.first()).thenReturn(testDocument).thenReturn(null);
//
//        cut.read();
//
//        Mockito.verify(testDocument, Mockito.times(1)).get(ID);
//        Mockito.verify(testDocument, Mockito.times(1)).getString(F_NAME);
//        Mockito.verify(testDocument, Mockito.times(1)).getString(L_NAME);
//        Mockito.verify(testDocument, Mockito.times(1)).get(AGE);
//        Mockito.verify(testDocument, Mockito.times(1)).get(CITY);
//    }

    @Test
    void updateTest() {
        try (MockedStatic<MongoClients> mockMongoClients = Mockito.mockStatic(MongoClients.class)) {
            mockMongoClients.when(() -> MongoClients.create(DB_URL)).thenReturn(client);
            Mockito.when(client.getDatabase(DB_NAME)).thenReturn(dataBase);
            Mockito.when(dataBase.getCollection(COLLECTION_NAME)).thenReturn(collection);
            int times = 1;

            cut.update(testPerson);

            Mockito.verify(collection, Mockito.times(times)).updateOne(eq(ID, testPerson.getId()), Updates.set(F_NAME, testPerson.getFname()));
            Mockito.verify(collection, Mockito.times(times)).updateOne(eq(ID, testPerson.getId()), Updates.set(L_NAME, testPerson.getLname()));
            Mockito.verify(collection, Mockito.times(times)).updateOne(eq(ID, testPerson.getId()), Updates.set(AGE, testPerson.getAge()));
            Mockito.verify(collection, Mockito.times(times)).updateOne(eq(ID, testPerson.getId()), Updates.set(CITY, testPerson.getCity()));
        }
    }

    @Test
    void updateNullPersonTest() {
        try (MockedStatic<MongoClients> mockMongoClients = Mockito.mockStatic(MongoClients.class)) {
            mockMongoClients.when(() -> MongoClients.create(DB_URL)).thenReturn(client);
            Mockito.when(client.getDatabase(DB_NAME)).thenReturn(dataBase);
            Mockito.when(dataBase.getCollection(COLLECTION_NAME)).thenReturn(collection);

            assertDoesNotThrow(() -> cut.update(null));
            Mockito.verifyNoInteractions(collection);
            Mockito.verifyNoInteractions(dataBase);
            Mockito.verifyNoInteractions(client);
        }
    }

    @Test
    void deleteTest() {
        try (MockedStatic<MongoClients> mockMongoClients = Mockito.mockStatic(MongoClients.class)) {
            mockMongoClients.when(() -> MongoClients.create(DB_URL)).thenReturn(client);
            Mockito.when(client.getDatabase(DB_NAME)).thenReturn(dataBase);
            Mockito.when(dataBase.getCollection(COLLECTION_NAME)).thenReturn(collection);
            int times = 1;

            cut.delete(testPerson);

            Mockito.verify(collection, Mockito.times(times)).deleteOne(eq(ID, testPerson.getId()));
        }
    }

    @Test
    void deleteNullPersonTest() {
        try (MockedStatic<MongoClients> mockMongoClients = Mockito.mockStatic(MongoClients.class)) {
            mockMongoClients.when(() -> MongoClients.create(DB_URL)).thenReturn(client);
            Mockito.when(client.getDatabase(DB_NAME)).thenReturn(dataBase);
            Mockito.when(dataBase.getCollection(COLLECTION_NAME)).thenReturn(collection);

            assertDoesNotThrow(() -> cut.delete(null));
            Mockito.verifyNoInteractions(collection);
            Mockito.verifyNoInteractions(dataBase);
            Mockito.verifyNoInteractions(client);
        }
    }
}