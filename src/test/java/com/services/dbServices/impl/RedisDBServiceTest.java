package com.services.dbServices.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.models.Person;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import redis.clients.jedis.Jedis;

import java.util.List;
import java.util.Map;

import static java.util.Map.entry;
import static org.junit.jupiter.api.Assertions.*;

class RedisDBServiceTest {

    private final String DB_NAME = "persons";
    private final Jedis jedis = Mockito.mock(Jedis.class);
    private final ObjectMapper mapper = Mockito.mock(ObjectMapper.class);
    private final Person personTest = new Person(123, "FFF", "LLL", 20, "CCC");
    private final String personInJson = "PersonInJSON";
    private final String value1 = "aaa";
    private final String value2 = "bbb";
    private final Map<String, String> mapTest = Map.ofEntries(
            entry("key1", value1),
            entry("key2", value2)
    );
    private final RedisDBService cut = new RedisDBService(jedis, mapper);

    @Test
    void createNullTest() {
        assertDoesNotThrow(() -> cut.create(null));
    }

    @Test
    void createTest() throws JsonProcessingException {
        Mockito.when(mapper.writeValueAsString(personTest)).thenReturn(personInJson);

        cut.create(personTest);

        Mockito.verify(jedis, Mockito.times(1)).hset(DB_NAME,
                String.valueOf(personTest.getId()), personInJson);
    }

    @Test
    void readTest() throws JsonProcessingException {
        Mockito.when(jedis.hgetAll(DB_NAME)).thenReturn(mapTest);
        Mockito.when(mapper.readValue(value1, Person.class)).thenReturn(personTest);
        Mockito.when(mapper.readValue(value2, Person.class)).thenReturn(personTest);
        List<Person> expected = List.of(personTest, personTest);

        List<Person> actual = cut.read();

        assertEquals(expected, actual);
    }

    @Test
    void updateNullTest() {
        assertDoesNotThrow(() -> cut.update(null));
    }

    @Test
    void updateTest() throws JsonProcessingException {
        Mockito.when(mapper.writeValueAsString(personTest)).thenReturn(personInJson);

        cut.update(personTest);

        Mockito.verify(jedis, Mockito.times(1)).hset(DB_NAME,
                String.valueOf(personTest.getId()), personInJson);
    }

    @Test
    void deleteNullTest() {
        assertDoesNotThrow(() -> cut.delete(null));
    }

    @Test
    void deleteTest() {
        cut.delete(personTest);

        Mockito.verify(jedis, Mockito.times(1)).hdel(DB_NAME, String.valueOf(personTest.getId()));
    }
}