package com;

import com.ui.StartUI;
import com.factories.DBServicesFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.models.Person;
import com.services.CommandService;
import com.services.PersonService;
import com.services.dbServices.IDBService;
import com.services.dbServices.impl.*;
import de.undercouch.bson4jackson.BsonFactory;
import org.apache.log4j.Logger;
import redis.clients.jedis.Jedis;
import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static final Logger LOGGER = Logger.getLogger("loggerFile");
    private static final String PATCH_TO_FILES = "src/main/java/com/files/";

    public static void main(String[] args) {
        IDBService[] services = createServices();
        List<Person> personCache = new ArrayList<>();
        PersonService personService = new PersonService(personCache);
        DBServicesFactory dbServicesFactory = new DBServicesFactory(services);
        CommandService commandService = new CommandService(personCache, personService, dbServicesFactory);
        StartUI app = new StartUI(commandService, personCache, personService);
        app.start();
        app.setLocationRelativeTo(null);
        app.setVisible(true);
        app.setResizable(false);
        app.setBounds(250, 250, 700, 500);
        app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private static IDBService[] createServices() {
        File jFile = createFile("JsonFileDB.json");
        File bFile = createFile("BinaryFileDB.binary");
        File xFile = createFile("XmlFileDB.xml");
        File yFile = createFile("YamlFileDB.yaml");
        File cFile = createFile("CsvFileDB.csv");
        ObjectMapper jObjectMapper = new ObjectMapper();
        ObjectMapper bObjectMapper = new ObjectMapper(new BsonFactory());
        XmlMapper xObjectMapper = new XmlMapper();
        ObjectMapper yObjectMapper = new ObjectMapper(new YAMLFactory());
        JsonDBService json = new JsonDBService(jFile, jObjectMapper);
        BinaryDBService binary = new BinaryDBService(bFile, bObjectMapper);
        XmlDBService xml = new XmlDBService(xFile, xObjectMapper);
        YamlDBService yaml = new YamlDBService(yFile, yObjectMapper);
        CsvDBService csv = new CsvDBService(cFile);
        PostgresqlDBService postgresql = new PostgresqlDBService();
        Jedis jedis = new Jedis("localhost", 6379);
        RedisDBService redis = new RedisDBService(jedis, jObjectMapper);
        MongoDBService mongo = new MongoDBService();
        IDBService[] services = {json, binary, xml, yaml, csv, postgresql, redis, mongo};
        return services;
    }

    private static File createFile(String name) {
        File file = new File(PATCH_TO_FILES + name);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                LOGGER.info("Problem with creating file  ");
                e.printStackTrace();
            }
        }
        return file;
    }
}
