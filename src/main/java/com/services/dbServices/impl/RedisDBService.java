package com.services.dbServices.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.models.Person;
import com.services.dbServices.IDBService;
import redis.clients.jedis.Jedis;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RedisDBService implements IDBService {

    private final String DB_NAME = "persons";
    private Jedis jedis;
    private ObjectMapper mapper;

    public RedisDBService(Jedis jedis, ObjectMapper mapper) {
        this.mapper = mapper;
        this.jedis = jedis;
    }

    @Override
    public void create(Person p) {
        if (p == null) {
            return;
        }

        String jPerson = null;
        try {
            jPerson = mapper.writeValueAsString(p);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        jedis.hset(DB_NAME, String.valueOf(p.getId()), jPerson);

    }

    @Override
    public List<Person> read() {
        List<Person> personList = new ArrayList<>();
        Person p = null;

        Map<String, String> map = jedis.hgetAll(DB_NAME);
        for (String s : map.values()) {
            try {
                p = mapper.readValue(s, Person.class);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            personList.add(p);
        }
        return personList;
    }

    @Override
    public void update(Person p) {
        create(p);
    }

    @Override
    public void delete(Person p) {
        if (p == null) {
            return;
        }
        jedis.hdel(DB_NAME, String.valueOf(p.getId()));
    }
}
