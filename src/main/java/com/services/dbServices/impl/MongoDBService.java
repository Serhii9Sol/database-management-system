package com.services.dbServices.impl;

import com.models.Person;
import com.mongodb.client.*;
import com.mongodb.client.model.Updates;
import com.services.dbServices.IDBService;
import org.bson.Document;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import static com.mongodb.client.model.Filters.eq;

public class MongoDBService implements IDBService {

    private static final String DB_URL = "mongodb://127.0.0.1:27017";
    private static final String DB_NAME = "mongo-database";
    private static final String COLLECTION_NAME = "persons";
    private static final String ID = "id";
    private static final String F_NAME = "fName";
    private static final String L_NAME = "lName";
    private static final String AGE = "age";
    private static final String CITY = "city";


    @Override
    public void create(Person p) {
        if (p == null) {
            return;
        }
        MongoCollection<Document> personsMongoCollection = getConnectionToMongoDB();
        Document document = new Document(Map.of(ID, p.getId(), F_NAME, p.getFname(), L_NAME, p.getLname(),
                AGE, p.getAge(), CITY, p.getCity()));
        personsMongoCollection.insertOne(document);
    }

    @Override
    public List<Person> read() {
        ArrayList<Person> bufferPerson = new ArrayList<>();
        MongoCollection<Document> personsMongoCollection = getConnectionToMongoDB();
        FindIterable<Document> documents = personsMongoCollection.find();
        for (Document readDocument : documents) {
            Person readPerson = null;
            try {
                readPerson = new Person(Integer.parseInt(String.valueOf(readDocument.get(ID))),
                        readDocument.getString(F_NAME),
                        readDocument.getString(L_NAME),
                        Integer.parseInt(String.valueOf(readDocument.get(AGE))),
                        readDocument.getString(CITY));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            bufferPerson.add(readPerson);
        }
        return bufferPerson;
    }

    @Override
    public void update(Person p) {
        if (p == null) {
            return;
        }
        MongoCollection<Document> personsMongoCollection = getConnectionToMongoDB();
        personsMongoCollection.updateOne(eq(ID, p.getId()), Updates.set(F_NAME, p.getFname()));
        personsMongoCollection.updateOne(eq(ID, p.getId()), Updates.set(L_NAME, p.getLname()));
        personsMongoCollection.updateOne(eq(ID, p.getId()), Updates.set(AGE, p.getAge()));
        personsMongoCollection.updateOne(eq(ID, p.getId()), Updates.set(CITY, p.getCity()));
    }

    @Override
    public void delete(Person p) {
        if (p == null) {
            return;
        }
        MongoCollection<Document> personsMongoCollection = getConnectionToMongoDB();
        personsMongoCollection.deleteOne(eq(ID, p.getId()));
    }

    private MongoCollection<Document> getConnectionToMongoDB() {
        MongoCollection<Document> personsMongoCollection = null;
        try (MongoClient mongoClient = MongoClients.create(DB_URL)) {
            MongoDatabase database = mongoClient.getDatabase(DB_NAME);
            personsMongoCollection = database.getCollection(COLLECTION_NAME);
        }
        return personsMongoCollection;
    }
}
