package com.services;

import com.factories.DBServicesFactory;
import com.models.Person;
import com.services.dbServices.IDBService;
import java.util.List;

public class CommandService {

    private List<Person> personCache;
    private IDBService currentDBService;
    private PersonService personService;
    private DBServicesFactory dbServicesFactory;

    public CommandService(List<Person> personCache, PersonService personService, DBServicesFactory dbServicesFactory) {
        this.personCache = personCache;
        this.personService = personService;
        this.dbServicesFactory = dbServicesFactory;
    }

    public void setCurrentDBService(int id) {
        currentDBService = dbServicesFactory.getDBService(id);
    }

    public void create(Person person) {
        currentDBService.create(person);
        personService.addPerson(person);
    }

    public void read() {
        List<Person> buff = currentDBService.read();
        personCache.clear();
        if (buff == null || buff.isEmpty()) {
            return;
        }
        personCache.addAll(buff);
    }

    public void update(Person person) {
        currentDBService.update(person);
    }

    public void delete(Person person) {
        currentDBService.delete(person);
        personService.deletePerson(person);
    }
}
