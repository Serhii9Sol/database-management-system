package com.models;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.opencsv.bean.CsvBindByName;
import java.util.Objects;

@JsonAutoDetect
public class Person {
    @CsvBindByName
    public int id;
    @CsvBindByName
    public String fname;
    @CsvBindByName
    public String lname;
    @CsvBindByName
    public int age;
    @CsvBindByName
    public String city;

    public Person(int id, String fname, String lname, int age, String city) {
        this.id = id;
        this.fname = fname;
        this.lname = lname;
        this.age = age;
        this.city = city;
    }

    public Person() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Person: id = " + id + " | Fname = " + fname + " | Lname = " + lname + " | Age=" + age + " | City = " + city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Person person = (Person) o;
        return id == person.id && age == person.age && Objects.equals(fname, person.fname) && Objects.equals(lname, person.lname) && Objects.equals(city, person.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fname, lname, age, city);
    }
}

