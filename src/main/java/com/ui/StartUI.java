package com.ui;

import com.models.Person;
import com.services.CommandService;
import com.services.PersonService;
import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class StartUI extends JFrame {

    private CommandService commandService;
    private List<Person> personCache;
    private PersonService personService;
    private int selectedDATA = -1;
    private JPanel jPanel = new JPanel();
    private String[] dataStore = {"JSON", "Binary", "XML", "YAML", "CSV", "PostgreSQL","Redis", "MongoDB"};
    private String[] columnNames = {"Id", "Name", "Lname", "Age", "City"};

    private int id = 0;
    private String fName = "";
    private String lName = "";
    private int age = 0;
    private String city = "";

    public StartUI(CommandService commandService, List<Person> personCache, PersonService personService) throws HeadlessException {
        this.commandService = commandService;
        this.personCache = personCache;
        this.personService = personService;
    }

    public void start() {
        super.add(jPanel);
        super.setTitle("Databases Management System");
        GridBagLayout gridBagLayout = new GridBagLayout();
        jPanel.setLayout(gridBagLayout);
        GridBagConstraints constraints2 = new GridBagConstraints();
        constraints2.weightx = 0;
        constraints2.weighty = 0.1;
        constraints2.gridx = 1;
        constraints2.gridy = 1;
        constraints2.gridheight = 1;
        constraints2.gridwidth = 1;
        GridBagConstraints constraints1 = new GridBagConstraints();
        constraints1.weightx = 0;
        constraints1.weighty = 0;
        constraints1.gridx = 0;
        constraints1.gridy = 0;
        constraints1.gridheight = 9;
        constraints1.gridwidth = 1;
        GridBagConstraints constraints3 = new GridBagConstraints();
        constraints3.weightx = 0;
        constraints3.weighty = 0.1;
        constraints3.gridx = 1;
        constraints3.gridy = 2;
        constraints3.gridheight = 1;
        constraints3.gridwidth = 1;
        GridBagConstraints constraints7 = new GridBagConstraints();
        constraints7.weightx = 0;
        constraints7.weighty = 0.1;
        constraints7.gridx = 1;
        constraints7.gridy = 0;
        constraints7.gridheight = 1;
        constraints7.gridwidth = 1;
        GridBagConstraints constraints8 = new GridBagConstraints();
        constraints8.weightx = 0;
        constraints8.weighty = 0.2;
        constraints8.gridx = 1;
        constraints8.gridy = 3;
        constraints8.gridheight = 1;
        constraints8.gridwidth = 1;
        GridBagConstraints constraints5 = new GridBagConstraints();
        constraints5.weightx = 0;
        constraints5.weighty = 0.2;
        constraints5.gridx = 1;
        constraints5.gridy = 4;
        constraints5.gridheight = 1;
        constraints5.gridwidth = 1;
        GridBagConstraints constraints4 = new GridBagConstraints();
        constraints4.weightx = 0;
        constraints4.weighty = 0.2;
        constraints4.gridx = 1;
        constraints4.gridy = 5;
        constraints4.gridheight = 1;
        constraints4.gridwidth = 1;
        GridBagConstraints constraints6 = new GridBagConstraints();
        constraints6.weightx = 0;
        constraints6.weighty = 0.2;
        constraints6.gridx = 1;
        constraints6.gridy = 6;
        constraints6.gridheight = 1;
        constraints6.gridwidth = 1;

        JComboBox boxDATA = new JComboBox(dataStore);
        JTable table = new JTable(renderTable());
        JScrollPane scrollPane = new JScrollPane(table);
        JButton readButton = new JButton("READ");
        JLabel  selectDataLabel = new JLabel("Select Database");
        JLabel  choseCommandLabel = new JLabel("Chose Command");
        JButton createButton = new JButton("Create new record");
        JButton updateButton = new JButton("Update record");
        JButton removeButton = new JButton("Remove record");

        table.setFillsViewportHeight(true);
        jPanel.add(boxDATA, constraints2);
        jPanel.add(selectDataLabel, constraints7);
        jPanel.add(choseCommandLabel, constraints8);
        jPanel.add(scrollPane, constraints1);
        jPanel.add(readButton, constraints3);
        jPanel.add(createButton, constraints5);
        jPanel.add(updateButton, constraints4);
        jPanel.add(removeButton, constraints6);

// МОДАЛЬНОЕ ОКНО
        JTextField idCreate = new JTextField();
        JTextField firstNameCreate = new JTextField();
        JTextField lastNameCreate = new JTextField();
        JTextField ageCreate = new JTextField();
        JTextField cityCreate = new JTextField();

        final JComponent[] inputsForModalCreate = new JComponent[] {
                new JLabel("ID"), idCreate,
                new JLabel("First name"), firstNameCreate,
                new JLabel("Last name"), lastNameCreate,
                new JLabel("Age"), ageCreate,
                new JLabel("City"), cityCreate,
        };

// READ BUTTON
        readButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectedDATA = boxDATA.getSelectedIndex();
                commandService.setCurrentDBService(selectedDATA);
                commandService.read();
                table.setModel(renderTable());
            }
        });

// CREATE BUTTON
        createButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (selectedDATA == -1) {
                    JOptionPane.showMessageDialog(null, "First select the database and click the button READ");
                    return;
                }
                if (0 == JOptionPane.showConfirmDialog(null, inputsForModalCreate, "Create record", JOptionPane.OK_CANCEL_OPTION)) {
                    fName = firstNameCreate.getText();
                    lName = lastNameCreate.getText();
                    city = cityCreate.getText();

                    try {
                        id = Integer.parseInt(idCreate.getText());
                        age = Integer.parseInt(ageCreate.getText());
                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(null, "id and age should be written in number");
                    }
                    if (!personService.validateId(id)) {
                        JOptionPane.showMessageDialog(jPanel, "INVALID ID");
                        return;
                    }
                    if (!personService.validateName(fName)) {
                        JOptionPane.showMessageDialog(jPanel, "INVALID FNAME");
                        return;
                    }
                    if (!personService.validateName(lName)) {
                        JOptionPane.showMessageDialog(jPanel, "INVALID LNAME");
                        return;
                    }
                    if (!personService.validateAge(age)) {
                        JOptionPane.showMessageDialog(jPanel, "INVALID AGE");
                        return;
                    }
                    if (!personService.validateName(city)) {
                        JOptionPane.showMessageDialog(jPanel, "INVALID CITY");
                        return;
                    }

                    Person createdPerson = personService.createPerson(id, fName, lName, age, city);
                    commandService.create(createdPerson);

                    JOptionPane.showMessageDialog(jPanel, "Person has been successfully created !\n" + createdPerson);

                    table.setModel(renderTable());
                }
                idCreate.setText("");
                firstNameCreate.setText("");
                lastNameCreate.setText("");
                ageCreate.setText("");
                cityCreate.setText("");
                refreshPaneldata();
            }
        });

// UPDATE BUTTON

        updateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    int rIndex = table.getSelectedRow();
                    int cIndex = 0;
                    Object ob = table.getValueAt(rIndex, cIndex);
                    int idFromTable = Integer.parseInt(ob.toString());

                    for (Person person : personCache) {
                        if ((person.getId() == idFromTable)) {

                            idCreate.setText(Integer.toString(person.getId()));
                            firstNameCreate.setText(person.getFname());
                            lastNameCreate.setText(person.getLname());
                            ageCreate.setText(Integer.toString(person.getAge()));
                            cityCreate.setText(person.getCity());

                            if (0 == JOptionPane.showConfirmDialog(null, inputsForModalCreate, "Update record", JOptionPane.OK_CANCEL_OPTION)) {

                                fName = firstNameCreate.getText();
                                lName = lastNameCreate.getText();
                                city = cityCreate.getText();

                                try {
                                    age = Integer.parseInt(ageCreate.getText());
                                } catch (Exception ex) {
                                    JOptionPane.showMessageDialog(null, "Age should be written in number");
                                }
                                if (!personService.validateId(id)) {
                                    JOptionPane.showMessageDialog(jPanel, "INVALID ID");
                                    return;
                                }
                                if (!personService.validateName(fName)) {
                                    JOptionPane.showMessageDialog(jPanel, "INVALID FNAME");
                                    return;
                                }
                                if (!personService.validateName(lName)) {
                                    JOptionPane.showMessageDialog(jPanel, "INVALID LNAME");
                                    return;
                                }
                                if (!personService.validateAge(age)) {
                                    JOptionPane.showMessageDialog(jPanel, "INVALID AGE");
                                    return;
                                }
                                if (!personService.validateName(city)) {
                                    JOptionPane.showMessageDialog(jPanel, "INVALID CITY");
                                    return;
                                }
                                person.setFname(fName);
                                person.setLname(lName);
                                person.setAge(age);
                                person.setCity(city);
                                commandService.update(person);
                                JOptionPane.showMessageDialog(jPanel, "Person has been successfully updated !");
                                table.setModel(renderTable());
                                break;
                            }
                        }
                    }
                    idCreate.setText("");
                    firstNameCreate.setText("");
                    lastNameCreate.setText("");
                    ageCreate.setText("");
                    cityCreate.setText("");
                    refreshPaneldata();
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "No person selected from the table");
                }
            }
        });

// REMOVE BUTTON

        removeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    int resultClientChoise;
                    int rIndex = table.getSelectedRow();
                    int cIndex = 0;
                    Object ob =  table.getValueAt(rIndex, cIndex);
                    int idFromTable = Integer.parseInt(ob.toString());
                    for (Person person : personCache) {
                        if ((person.getId() == idFromTable)) {
                            resultClientChoise = JOptionPane.showConfirmDialog(jPanel, person + "\n Are you sure you want to delete this person ?", "delete confirmation", JOptionPane.YES_NO_OPTION);
                            if (resultClientChoise == 0) {
                                commandService.delete(person);
                            }
                            break;
                        }
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "No person selected from the table");
                }
                table.setModel(renderTable());
            }
        });
        jPanel.revalidate();
    }


    private TableModel renderTable() {
        return new AbstractTableModel() {
            Object rowData[][] = fillTable(personCache);  // personCahse
            String columnNames[] = {"Id", "Name", "Lname", "Age", "City"};
            public int getColumnCount() {
                return columnNames.length;
            }
            public String getColumnName(int column) {
                return columnNames[column];
            }
            public int getRowCount() {
                return rowData.length;
            }
            public Object getValueAt(int row, int column) {
                return rowData[row][column];
            }
            public Class getColumnClass(int column) {
                return (getValueAt(0, column).getClass());
            }
            public void setValueAt(Object value, int row, int column) {
                rowData[row][column] = value;
            }
            public boolean isCellEditable(int row, int column) {
                return (column != 4);
            }
        };
    }

    private  String[][] fillTable(List<Person> dataUpdate) {
        String[][] data = new String[dataUpdate.size()][columnNames.length];
        int i = 0;
        for (Person myPerson : dataUpdate) {
            data[i][0] = String.valueOf(myPerson.getId());
            data[i][1] = myPerson.getFname();
            data[i][2] = myPerson.getLname();
            data[i][3] = String.valueOf(myPerson.getAge());
            data[i][4] = myPerson.getCity();
            i++;
        }
        return data;
    }

    private void refreshPaneldata() {
        id = 0;
        fName = "";
        lName = "";
        age = 0;
        city = "";
    }
}


